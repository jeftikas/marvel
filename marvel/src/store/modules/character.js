import axios from 'axios'
import public_key from '../../marvel'

const state = () => ({
    character: [],
    name: "",
  });
  
  
  // actions
const actions = {
    
  
  }
  
  
  // mutations
const mutations = {
  getCharacterforName(state, name) {
    
    
    // let name = this.state.name
    console.log(name);
     axios
      .get(
        `http://gateway.marvel.com/v1/public/characters?name=${name}&apikey=${public_key}`
      )
      .then((result) => {
        console.log(result);
        this.state.character = result
      })
      .catch((error) => {
        console.log(error);
      });

      
  },
   
  }
  
  
  export default {
    namespaced: true,
    state,
    // getters,
    actions,
    mutations
  }